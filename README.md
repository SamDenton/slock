**This build of slock of part of SÅMDE, it's a arch linux post installation script that recreates my desktop setup. I highly recommend checking it out : [SÅMDE](https://gitlab.com/SamDenton/samde)**

# SÅM's build of slock
[Slock](https://tools.suckless.org/slock) is a simple x display locker made by suckless. I've applied a few patches to implement the features that I want.

## Patches

- capscolor
- xresources

## Installation
```
git clone --depth 1 https://gitlab.com/SamDenton/slock.git
cd dmenu
sudo make install
```
